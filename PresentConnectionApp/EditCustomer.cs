﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentConnectionApp

{
    public partial class EditCustomer : Form
    {
        CustomerList custlistForm;
        Customer cust;

        public EditCustomer(Customer customer, CustomerList customerList)
        {
            
            InitializeComponent();
            cust = customer;
            custlistForm = customerList;

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void EditCustomer_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            cust.Name = textBox1.Text;
            cust.CustNum = int.Parse(textBox2.Text);
            custlistForm.loadList();

        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "New Customer Name")
            {
                textBox1.Text = "";
            }
        }
  
        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox1.Text = "New Customer Name";
            }
        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Text == "New Customer Number")
            {
                textBox2.Text = "";
            }
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                textBox2.Text = "New Customer Number";
            }
        }
    }
}
