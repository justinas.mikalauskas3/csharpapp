﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentConnectionApp
{
    public class Customer
    {
        public string Name { get; set; }
        public int CustNum { get; set; }
        public List<Order> Orders { get; set; } = new List<Order>();

        public override
                string ToString() => $"{CustNum} : {Name}"; // $ = string.Format("{0} : {1}", CustNum, Name);

    }
}
