﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentConnectionApp
{
    public partial class AddCustomer : Form
    {
       
        CustomerList custlistForm;

        public AddCustomer(CustomerList customerList)
        {
            InitializeComponent();
            custlistForm = customerList;
        }


    private void button1_Click(object sender, EventArgs e)
        {
            
            if (!String.IsNullOrEmpty(textBox1.Text) && !(textBox1.Text == "Customer Name") && int.TryParse(textBox2.Text, out int inCustNum))
            {       
                if (!customerExist(custlistForm.Customers, inCustNum))
                {
                    custlistForm.Customers.Add(new Customer { Name = textBox1.Text, CustNum = inCustNum });
                    custlistForm.loadList();
                }
                else
                {
                    string text = "Customer number already exists";
                    MessageBox.Show(text);
                }
            }
            else {
                string text = "Wrong Customer name or number";
                MessageBox.Show(text);
            }


        }

        private void AddCustomer_Load(object sender, EventArgs e)
        {
            
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "Customer Name") {
                textBox1.Text = "";
            }
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox1.Text = "Customer Name";
            }
        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Text == "Customer Number")
            {
                textBox2.Text = "";
            }
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                textBox2.Text = "Customer Number";
            }
        }

        private bool customerExist(List<Customer> custList, int custNum) => custList.Any(c => c.CustNum == custNum);

    }
}
