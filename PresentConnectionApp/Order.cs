﻿using System.Collections.Generic;

namespace PresentConnectionApp
{
    public class Order
    {

        public int OrderNum { get; set; }
        public List<Item> Items { get; set; }


        public override

        //string ToString() => $"{OrderNum} : {string.Join( ", ", Items)}";
        string ToString() => $"{OrderNum} : {string.Join(" ", Items)}";
            
    }
}