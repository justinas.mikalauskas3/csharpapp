﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentConnectionApp
{
    public partial class CustomerOrderList : Form
    {

        private readonly Customer customer;

        public CustomerOrderList()
        {
            InitializeComponent();
        }

        public CustomerOrderList(Customer customer) : this() {
            this.customer = customer;
        }

        private void CustomerOrderList_Load(object sender, EventArgs e)
        {
            loadList();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button3.Enabled = listBox1.SelectedIndex >= 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Order newOrder = new Order()
            {
                OrderNum = customer.Orders.Max(n => n.OrderNum + 1)
            };
        
        

        List<Item> newItemList = new List<Item>();

            foreach (var listBoxItem in listBox2.Items)
            {
                newItemList.Add(new Item { Name = listBoxItem.ToString() });
            }

            newOrder.Items = newItemList;
            customer.Orders.Add(newOrder);

            listBox2.Items.Clear();
            loadList();

        }

        private void loadList() {
            listBox1.Items.Clear();
            foreach (Order order in customer.Orders)
            {
                listBox1.Items.Add(order);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            customer.Orders.RemoveAt(listBox1.SelectedIndex);
            loadList();
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "New Order")
            {
                textBox1.Text = "";
            }
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "New Order")
            {
                textBox1.Text = "";
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox2.Items.Add(textBox1.Text);
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

