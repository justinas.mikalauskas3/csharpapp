﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentConnectionApp
{
    public partial class CustomerList : Form
    {
        

     public   List<Customer> Customers = JsonConvert.DeserializeObject<List<Customer>>(new StreamReader("Data.json").ReadToEnd());

        AddCustomer addCustomer;
        EditCustomer editCustomer;
        public CustomerList()
        {
            CustomerList custlist;
            InitializeComponent();
            
            
        }

        private void CustomerList_Load(object sender, EventArgs e)
        {
            loadList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var AddCust = new AddCustomer(this);
            AddCust.ShowDialog();
            loadList();

        }

        public void loadList() {
            listBox1.Items.Clear();
            foreach (Customer cust in Customers)
            {
                listBox1.Items.Add(cust);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var newList = new CustomerOrderList(Customers[listBox1.SelectedIndex]);
            newList.ShowDialog();
            
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
                button2.Enabled = button3.Enabled = button4.Enabled = listBox1.SelectedIndex >= 0;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var editCust = new EditCustomer(Customers[listBox1.SelectedIndex], this);
            editCust.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Customers[listBox1.SelectedIndex].Orders.Count > 0) {
                string text = "Can't delete customer that has orders";
                MessageBox.Show(text);
            } else
                Customers.RemoveAt(listBox1.SelectedIndex);
            loadList();
        }
    }
}
